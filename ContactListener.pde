import org.jbox2d.callbacks.ContactImpulse;
import org.jbox2d.callbacks.ContactListener;
import org.jbox2d.collision.Manifold;
import org.jbox2d.dynamics.contacts.Contact;

class CustomListener implements ContactListener {

    CustomListener() {

    }



    void beginContact(Contact cp){

        Box box = null;
        Bucket bucket = null;

        Fixture fixture1 = cp.getFixtureA();
        Fixture fixture2 = cp.getFixtureB();

        Body body1 = fixture1.getBody();
        Body body2 = fixture2.getBody();

        Object obj1 = body1.getUserData();
        Object obj2 = body2.getUserData();

        if(obj1 instanceof Box && obj2 instanceof Bucket){
            box = (Box) obj1;
            bucket = (Bucket) obj2;
        
        } else if(obj1 instanceof Bucket && obj2 instanceof Box) {
            bucket = (Bucket) obj1;
            box = (Box) obj2;
        
        }

        if(box != null && bucket != null){
            if(box.type == bucket.type){
                if(box.getPos().y > bucket.getPos().y){
                    box.done = true;
                }
                
            } else {
                // println("Wrong box bucket!");
            }
        }
    }

    void endContact(Contact contact){
        //Nothing
    }

    void preSolve(Contact contact, Manifold oldManifold){
        //Nothing
    }

    void postSolve(Contact contact, ContactImpulse impulse){
        //Nothing
    }

}
