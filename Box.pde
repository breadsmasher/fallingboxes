class Box {

    Body body;
    float bWidth;
    float bHeight;
    boolean pressed = false;
    boolean done = false;

    Types type;

    Box(){
        this.bWidth = random(1.0f, 5.0f);
        this.bHeight = bWidth;
        float x = random(0, width);
        float y = random(0, height);
        makeBody(new Vec2(x, y), bWidth, bHeight);
    }

    Box(float x, float y, float width, float height, Types type){
        this.bWidth = random(20, width);
        this.bHeight = bWidth;
        this.type = type;
        makeBody(new Vec2(x, y), bWidth, bHeight);
    }

    void killBody(){
        done = true;
        box2d.destroyBody(body);
    }

    boolean contains(float x, float y){
        Vec2 worldPoint = box2d.coordPixelsToWorld(x, y);
        Fixture f = body.getFixtureList();
        boolean inside = f.testPoint(worldPoint);
        return inside;
    }

    boolean done(){
        Vec2 pos = box2d.getBodyPixelCoord(body);
        if(pos.y > height){
            killBody();
            return true;
        }
        return false;
    }

    void display(){
        
        Vec2 pos = box2d.getBodyPixelCoord(body);
        
        rectMode(PConstants.CENTER);
        
        float a = body.getAngle();
        
        pushMatrix();
        translate(pos.x, pos.y);
        rotate(-a);
        fill(type.hColour(), type.sColour(), type.bColour());
        stroke(0);
        rect(0,0,bWidth,bHeight);
        popMatrix();
    }


    void makeBody(Vec2 centre, float width, float height){
        BodyDef bd = new BodyDef();
        bd.type = BodyType.DYNAMIC;
        bd.position.set(box2d.coordPixelsToWorld(centre));
        bd.setBullet(true);
        body = box2d.createBody(bd);
        body.setUserData(this);

        PolygonShape ps = new PolygonShape();
        float box2dW = box2d.scalarPixelsToWorld(width/2);
        float box2dH = box2d.scalarPixelsToWorld(height/2);
        ps.setAsBox(box2dW, box2dH);

        FixtureDef fd =  new FixtureDef();
        fd.shape = ps;
        fd.density = bWidth * bHeight;
        fd.friction = 0.3;
        fd.restitution = 0.6;

        body.createFixture(fd);
        body.setLinearVelocity(new Vec2(random(-10, 10), random(4, 10)));
        body.setAngularVelocity(random(0, 5));
    }

    Vec2 getPos(){
        return box2d.getBodyPixelCoord(body);
    }

    void applyAForce(Vec2 force){
        body.applyForceToCenter(force);
    }
}