class Bucket {

    float bWidth;
    float bHeight;
    float radius = 50;
    Body body;
    Types type;

    Bucket(float x, float y, float bWidth, float bHeight, Types type){
        this.bWidth = bWidth;
        this.bHeight = bHeight;
        this.type = type;
        makeBody(x, y);
    }

    // void killBody(){
    // }

    void display(){
        Vec2 pos = box2d.getBodyPixelCoord(body);
        float a = body.getAngle();

        rectMode(CENTER);
        pushMatrix();
        translate(pos.x, pos.y);
        // rotate(-a);
        fill(type.hColour(), type.sColour(), type.bColour());
        stroke(0);

        Fixture f = body.getFixtureList();
        while(f != null){
            PolygonShape ps = (PolygonShape) f.getShape();
            beginShape();
            
            for(int i = 0; i < ps.getVertexCount(); i++){
                Vec2 v = box2d.vectorWorldToPixels(ps.getVertex(i));
                vertex(v.x, v.y);
            }
            
            endShape(CLOSE);
            f = f.getNext();
        
        }
        
        popMatrix();
    }   

    boolean contains(float x, float y){
        Vec2 worldPoint = box2d.coordPixelsToWorld(x, y);
        Fixture f = body.getFixtureList();
        boolean contains = false;
        while(f != null && !contains){
            contains = f.testPoint(worldPoint);
            f = f.getNext();
        }

        return contains;
    }

    void makeBody(float x, float y){

        float innerWidth = map(bWidth, 1, width, 1, 20);

        Vec2 centre = new Vec2(x, y);

        BodyDef bd = new BodyDef();
        bd.type = BodyType.STATIC;
        bd.position.set(box2d.coordPixelsToWorld(centre));
        bd.setFixedRotation(true);
        body = box2d.createBody(bd);
        body.setUserData(this);

        PolygonShape psLeft = new PolygonShape();
            Vec2[] verticesLeft = new Vec2[4];
            verticesLeft[0] = box2d.vectorPixelsToWorld(new Vec2(-bWidth/2, -bHeight/2));
            verticesLeft[1] = box2d.vectorPixelsToWorld(new Vec2(-bWidth/2, (-bHeight/2) + bHeight));
            verticesLeft[2] = box2d.vectorPixelsToWorld(new Vec2(-bWidth/2 + innerWidth, (-bHeight/2) + bHeight));
            verticesLeft[3] = box2d.vectorPixelsToWorld(new Vec2(-bWidth/2 + innerWidth, -bHeight/2));
            psLeft.set(verticesLeft, verticesLeft.length);
            body.createFixture(psLeft, 1.0);

        PolygonShape psRight = new PolygonShape();
            Vec2[] verticesRight = new Vec2[4];
            verticesRight[0] = box2d.vectorPixelsToWorld(new Vec2(bWidth/2 - innerWidth, -bHeight/2));
            verticesRight[1] = box2d.vectorPixelsToWorld(new Vec2(bWidth/2 - innerWidth, -bHeight/2 + bHeight));
            verticesRight[2] = box2d.vectorPixelsToWorld(new Vec2(bWidth/2, -bHeight/2 + bHeight));
            verticesRight[3] = box2d.vectorPixelsToWorld(new Vec2(bWidth/2, -bHeight/2));
            psRight.set(verticesRight, verticesRight.length);
            body.createFixture(psRight, 1.0);

        PolygonShape psBottom = new PolygonShape();
            Vec2[] verticesBottom = new Vec2[4];
            verticesBottom[0] = box2d.vectorPixelsToWorld(new Vec2(-bWidth/2, bHeight/2));
            verticesBottom[1] = box2d.vectorPixelsToWorld(new Vec2(-bWidth/2, bHeight/2 + innerWidth));
            verticesBottom[2] = box2d.vectorPixelsToWorld(new Vec2(bWidth/2, bHeight/2 + innerWidth));
            verticesBottom[3] = box2d.vectorPixelsToWorld(new Vec2(bWidth/2, bHeight/2));
            psBottom.set(verticesBottom, verticesBottom.length);
            body.createFixture(psBottom, 1.0);

        
    }

    Vec2 getPos(){
        return box2d.getBodyPixelCoord(body);
    }

    // void applyAForce(Vec2 force){
    // }

    

}