class Solid {

    Body body;
    float bWidth;
    float bHeight;

    boolean pressed = false;
    boolean drawable = true;

    Solid(float x, float y, float width, float height, boolean drawable){
        this.bWidth = width;
        this.bHeight = height;
        this.drawable = drawable;
        if(drawable){
            makeBody(new Vec2(x, y), bWidth, bHeight);
        }
    }

    void killBody(){
        box2d.destroyBody(body);
    }

    boolean contains(float x, float y){
        Vec2 worldPoint = box2d.coordPixelsToWorld(x, y);
        Fixture f = body.getFixtureList();
        boolean inside = f.testPoint(worldPoint);
        return inside;
    }

    void display(){
        if(drawable){
            Vec2 pos = box2d.getBodyPixelCoord(body);
            rectMode(PConstants.CENTER);
            pushMatrix();
            translate(pos.x, pos.y);
            fill(175);
            stroke(0);
            rect(0,0,bWidth,bHeight);
            popMatrix();
        }
    }

    void makeBody(Vec2 centre, float width, float height){
        BodyDef bd = new BodyDef();
        bd.type = BodyType.STATIC;
        bd.position.set(box2d.coordPixelsToWorld(centre));
        body = box2d.createBody(bd);

        PolygonShape sd = new PolygonShape();
        float box2dW = box2d.scalarPixelsToWorld(width/2);
        float box2dH = box2d.scalarPixelsToWorld(height/2);
        sd.setAsBox(box2dW, box2dH);

        FixtureDef fd =  new FixtureDef();
        fd.shape = sd;
        fd.density = 10;
        fd.friction = 0.3;
        fd.restitution = 0.5;

        body.createFixture(fd);
    }


    Vec2 getPos(){
        return box2d.getBodyPixelCoord(body);
    }
}