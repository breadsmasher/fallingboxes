import shiffman.box2d.*;
import org.jbox2d.common.*;
import org.jbox2d.dynamics.joints.*;
import org.jbox2d.collision.shapes.*;
import org.jbox2d.collision.shapes.Shape;
import org.jbox2d.common.*;
import org.jbox2d.dynamics.*;

Box2DProcessing box2d;

Controller controller;

ArrayList<Solid> walls;
ArrayList<Box> boxes;
ArrayList<Bucket> buckets;

float maxBucketWidth = 150;
float bucketWidth;
float bucketHeight;
float segmentWidth;
float maxWidth;

HScrollbar hs1;
HScrollbar hs2;
HScrollbar hs3;

int maxBuckets = 1;
int score = 0;

Types types[] = Types.values();

void setup(){
    colorMode(HSB);
    size(500,800);
    // fullScreen();
    smooth();
    
    box2d = new Box2DProcessing(this);
    box2d.createWorld();
    box2d.setGravity(0, -40);
    box2d.world.setContactListener(new CustomListener());
   
    // hs1 = new HScrollbar(width/2, height - 40, width/2, 10, 16);
    // hs2 = new HScrollbar(width/2, height - 25, width/2, 10, 16);
    // hs3 = new HScrollbar(width/2, height - 10, width/2, 10, 16);

    segmentWidth = width / maxBuckets;
    maxWidth = width / maxBuckets;
    bucketWidth = maxWidth - (10 * maxBuckets);
    
    if(bucketWidth > maxBucketWidth){
        bucketWidth = maxBucketWidth;
    }
    
    bucketHeight = bucketWidth - (bucketWidth / maxBuckets) - (10 * maxBuckets);

    boxes = new ArrayList<Box>();
    walls = new ArrayList<Solid>();
    buckets = new ArrayList<Bucket>();

    for(int i = 0; i < types.length; i++){
        int segment = i + 1;
        buckets.add(new Bucket(((segmentWidth/2) * i) + (segmentWidth / 2) * segment, height - (height / 10), bucketWidth, 90, types[i]));
    }

    controller = new Controller();
}   

void pressed(){
    for(Box box : boxes){
        if(box.contains(mouseX, mouseY) && controller.controller == null){
            controller.bind(mouseX, mouseY, box);
            box.pressed = true;
        }
    }
}

void mouseReleased(){
    controller.destroy();
}   

void draw(){
    if(frameCount % 60 == 0){
        float size = random(20, bucketWidth/3);
        boxes.add(new Box(random(0, width), 0, size, size, types[floor(random(0, types.length))]));
    }
    box2d.step();
    
    if(mousePressed){
        pressed();
    }

    // float hColor = map(hs1.getPos(), hs1.sposMin, hs1.sposMax,1,360);
    // float sColor = map(hs2.getPos(), hs2.sposMin, hs2.sposMax,1,360);
    // float bColor = map(hs3.getPos(), hs3.sposMin, hs3.sposMax,1,360);

    // background(hColor,sColor,bColor);
    background(48,66,148);

    for(Solid s : walls){
        s.display();
    }

    for(int i = boxes.size() - 1; i >= 0; i--){
        Box b = boxes.get(i);
        if(b.done() || b.done){
            boxes.remove(i);
        }

        if(b.pressed && !mousePressed){
            b.pressed = false;
        }

        b.display();
    }

    for(Bucket b : buckets){
        b.display();
    }

    controller.update(mouseX, mouseY);
    controller.display();
    
    // hs1.update();
    // hs1.display();
    
    // hs2.update();
    // hs2.display();
    
    // hs3.update();
    // hs3.display();

    textSize(20);
    textAlign(10);
    // filter(INVERT);
    fill(255);
    text(score, 20, 30);
    // text(floor(frameRate), 20, 70);
    // text(boxes.size(), 20, 110);
    // text(floor(hColor) + "," + floor(sColor) + "," + floor(bColor), 20, 150);
    
}