class Controller {

    MouseJoint controller;

    Controller(){
        controller = null;
    }
    
    void update(float x, float y){
        if(controller != null){
            Vec2 mouseWorld = box2d.coordPixelsToWorld(x, y);
            controller.setTarget(mouseWorld);
        }
    }

    void display(){
        if(controller != null){
            Vec2 v1 = new Vec2(0, 0);
            Vec2 v2 = new Vec2(0, 0);

            controller.getAnchorA(v1);
            controller.getAnchorB(v2);

            v1 = box2d.coordWorldToPixels(v1);
            v2 = box2d.coordWorldToPixels(v2);

            stroke(0);
            strokeWeight(1);
            line(v1.x, v1.y, v2.x, v2.y);
        }
    }

    void bind(float x, float y, Box box){
        MouseJointDef md = new MouseJointDef();
        md.bodyA = box2d.getGroundBody();
        md.bodyB = box.body;

        Vec2 mp = box2d.coordPixelsToWorld(x, y);
        md.target.set(mp);
        md.maxForce = 10000.0f * box.body.m_mass;
        md.frequencyHz = 8.0;
        md.dampingRatio = 0.9;

        controller = (MouseJoint) box2d.world.createJoint(md);

    }

    void destroy(){
        if(controller != null){
            box2d.world.destroyJoint(controller);
            controller = null;
        }
    }
}