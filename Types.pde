enum Types {

    RED (294, 337, 97),
    BLUE(165,327,189),
    YELLOW(38,327,189),
    GREEN(60,327,189),
    PURPLE(212,372,143);

    float hColour;
    float sColour;
    float bColour;

    Types(float hColour, float sColour, float bColour){
        this.hColour = hColour;
        this.sColour = sColour;
        this.bColour = bColour;
    }

    float hColour(){
        return this.hColour;
    }

    float sColour(){
        return this.sColour;
    }

    float bColour(){
        return this.bColour;
    }

    void setHColour(float hColour){
        this.hColour = hColour;
    }

    void setSColour(float sColour){
        this.sColour = sColour;
    }

    void setBColour(float bColour){
        this.bColour = bColour;
    }

}